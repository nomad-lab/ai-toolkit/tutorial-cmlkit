# analytics-tutorial-cmlkit

Tutorial for [`cmlkit`](https://github.com/sirmarcel/cmlkit).

In addition to the prerequisites in `setup.py`, this tutorial requires [`qmmlpack`](https://gitlab.com/qmml/qmmlpack/-/tree/development) on the DEVELOPMENT branch.

It also requires the environment variables `CML_PLUGINS=cscribe` and `CML_DATASET_PATH=data/cmlkit` to be set.

It also requires the `nglview` and `ipywidgets` `nbextensions` to be available. (But only for an appendix, don't sweat it.)