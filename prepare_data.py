# Used to prepare the example datasets for hyper-parameter optimisation

import numpy as np

np.random.seed(123)
import cmlkit

data = cmlkit.load_dataset("nmd18_train")

rest, train, test = cmlkit.utility.threeway_split(data.n, 800, 200)

train = cmlkit.dataset.Subset.from_dataset(data, idx=train, name="nmd18_hpo_train")
print(train.n)
train.save(directory="data/cmlkit")
test = cmlkit.dataset.Subset.from_dataset(data, idx=test, name="nmd18_hpo_test")
print(test.n)
test.save(directory="data/cmlkit")
